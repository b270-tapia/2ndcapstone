const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");




// User registration

router.post("/checkEmail", userController.checkEmailExists);
router.post("/register", userController.registerUser);
 
// User's authentication
router.post("/login", userController.loginUser)

router.post("/order", userController.order)

router.get("/details", auth.verify, userController.getUserDetails)
router.get("/:userId",  auth.verify, userController.getUser)

router.get("/allOrders", auth.verify, userController.getAllOrders)

// router.get("/:id/order", userController.getUserOrders)

// router.put("/:id", auth.verify, (req, res) => {
// 	userController.updateUser(req.params.id, req.body).then(result => res.send(result));
// })

router.put("/:userId", auth.verify, userController.updateUser)

// router.patch("/:id", auth.verify, userController.newAdmin)
module.exports  = router;