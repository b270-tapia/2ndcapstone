const express = require("express")
const router = express.Router()
const cartController = require("../controllers/cartController")
const auth = require("../auth");


router.get('/', cartController.getAllCarts)
router.get('/cart', cartController.getSingleCart)
router.get('/user/:userId', cartController.getCartsbyUserId)

router.post('/addCart', cartController.addCart)
//router.post('/:id',cart.addtoCart)

router.put('/:id', cartController.editCart)
// router.patch('/:id', cartController.editCart)
router.delete("/:id", (req, res) => {
	cartController.deleteCart(req.params.id).then(result => res.send(result))
})
module.exports = router;