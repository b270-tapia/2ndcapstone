const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");
const multer = require("multer");
const fs = require('fs');




const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, 'uploads/');

	},

	filename: function (req, file, cb) {
		cb(null, file.originalname);
	},

	fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }

}});


const upload = multer({storage: storage});




// Routes for adding/creating a product
router.post("/", auth.verify, upload.single('productImage'), productController.addProduct)
// Routes for getting all products
router.get("/all", productController.getAllProducts)
// Routes for getting all active products
router.get("/allActive", productController.getAllActive);

router.get("/:productId", productController.getProduct);
//  Updating product information
router.put("/:productId", productController.updateProduct);
// Routes for archiving a product
router.put("/:productId/archive", auth.verify, productController.archiveProduct);
// router.put("/:productId/unarchive", auth.verify, productController.unarchiveProduct);


module.exports  = router;

