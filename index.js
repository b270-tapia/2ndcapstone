const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors")
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const cartRoutes = require("./routes/cartRoutes")
const multer = require('multer');
const bodyParser = require('body-parser')
// const ejs = require('ejs');
const path = require('path')





const app = express();

// DB Connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.3aoorzt.mongodb.net/E-commerceAPI?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}	
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log(`We are connected to the cloud database`));

// EJS
// app.set('view engine', 'ejs');

// Public folder
app.use('/uploads', express.static('uploads'));

// Middlewares
// app.get('/', (req, res) => res.render('index'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended : true }));

app.use(bodyParser.urlencoded({ extended : false }));
app.use(bodyParser.json());
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/carts", cartRoutes);


app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})