const mongoose = require("mongoose");

let userSchema = new mongoose.Schema({


	firstName: {
		type: String,
		// required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		// required: [true, "Last name is required"]
	},
	
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	mobileNumber: {
		type: String,
		// required: [true, "Mobile number is required"]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{	
			
			productName: {
				type: String,
				required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				// required: [true, "Quantity is required"]
			},
			totalAmount: {
				type: Number,
				// required: ""
			},
		}
	],
	
	purchasedOn : {
		type: Date,
		default: new Date()
	}
	
});

module.exports = mongoose.model("User", userSchema);