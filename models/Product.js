const mongoose = require("mongoose");

let productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	size: {
		type: String,
		required: [true, "Size is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	productImage: {
		_id: mongoose.Schema.Types.ObjectId,
		type: String,
		// required: [true, "Product Image is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			orderId: {
				type: String,
				// required: [true, "Order Id is required"]
			},
			userId: {
				type: String,
				
			}
		} 

	]
	
});

module.exports = mongoose.model("Product", productSchema);

