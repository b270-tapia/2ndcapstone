const mongoose = require("mongoose");
const Product = require("../models/Product")
const User = require("../models/User")

const cartSchema = new mongoose.Schema({
    userId:{
        type: String,
        required: true
    },
    date:{
        type: Date,
        default: new Date()
    },
   products:[
        {
            productName: {
                type: String,
                required:true
            },
            productId:{
                type: String,
                ref: Product,
                required:true
            },
            quantity:{
                type:Number,
                required:true
            }
        }
   ]
})

module.exports = mongoose.model("Cart", cartSchema)