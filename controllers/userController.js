const User = require("../models/User");
const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");




// User Registration

module.exports.checkEmailExists = (req, res) => {
	return User.find({email: req.body.email}).then(result => {

		// The "find" method return a record if a match is found
		if(result.length > 0) {
			return res.send (true);

		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return res.send (false);
		}
	})
	.catch(error => res.send (error))
}

module.exports.getAllUsers = (req, res) => {
	return User.find({}).then(result => res.send(result));
};

module.exports.registerUser = (req, res) => {

	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,

		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		// Syntax: bcrypt.hashSync(dataToBeEncrypted, saltRounds)
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	// Saves the created object to our database
	return newUser.save().then(user => {
		console.log(user);
		res.send(true)
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
}

// Login User
module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email}).then(result => {
		
		if(result == null) {
			return res.send({message: "No user found"});
	
		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			if(isPasswordCorrect) {

				return res.send({accessToken: auth.createAccessToken(result)});
			} else {
				return res.send(false)
			}
		}
	})
};


module.exports.order = async (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	if(!userData.isAdmin) {
		let productName = await Product.findById(req.body.productId).then(result => result.name);
		
		let data = {
		
			userId: userData.id,
			email: userData.email,
			totalAmount: req.body.totalAmount,
			quantity: req.body.quantity,
			productId: req.body.productId,
			productName: productName,
			
			

		}

		console.log(data)


		let isUserUpdated = await User.findById(data.userId).then(user => {
			

			user.orders.push({
				
				productName: data.productName,
				quantity: data.quantity,
				totalAmount: data.totalAmount,
				
				
				
			});
			


			return user.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false
			})
		});

	
		console.log(isUserUpdated);

		let isProductUpdated = await Product.findById(req.body.productId).then(product => {
		
			product.orders.push({
				orderId: req.body.orderId,
				userId: data.userId
			});


			return product.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});

		console.log(isProductUpdated);

		

		if(isUserUpdated && isProductUpdated) {
			return res.send(true);

		}else {
			return res.send(false);
		}
	} else {
		return res.send(false)
	};
	
}

module.exports.getUserDetails = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {
		result.password = "";

		return res.send(result)
	})
};

module.exports.getUser = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(error)
	})

};

// module.exports.getUserOrders = (req, res) => {
// 	const orders = req.query.orders.split(';');
// 	console.log(orders)
// }

module.exports.getAllOrders = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	if(userData.isAdmin){
		
		return User.find({}).then(result => res.send(result))

	}

}
// module.exports.updateUser = (req, res) => {
// 	const userData = auth.decode (req.headers.authorization);
// 	if(userData.isAdmin) {
// 		let updateUser = {

// 			isAdmin: req.body.isAdmin
// 		};

// 		return User.findByIdAndUpdate(req.body.userId, updateUser, {new: true}).then(result => {
// 			console.log(result);
// 			return res.send(result)
// 		})
// 		.catch(error => {
// 			console.log(error);
// 			return res.send(false)
// 		})
// 	} else {
// 		return res.send(false)
		
// 	}
// };

// module.exports.newAdmin = (req, res) => {
// 	const userData = auth.decode(req.headers.authorization);
// 	if(userData.isAdmin) {
// 		let newAdmin = {
// 			isAdmin: req.body.isAdmin
// 		}
// 		return User.findByIdAndUpdate(req.params.userId, newAdmin).then(result => {
// 			console.log(result)
// 			return res.send(result)
// 		})
// 		.catch(error => {
// 			console.log(error);
// 			res.send(false)
// 		})
// 	} else {
// 		return res.send(false)
// 	}
// }

// module.exports.updateUser = (req, res) => {
// 	const userData = auth.decode(req.headers.authorization);
// 	if(userData.isAdmin) {
// 		let updateUser = {

// 			userId: req.body.userId,
// 			isAdmin: req.body.isAdmin
// 		}
// 		return User.findById(req.body.userId, updateUser).then(result => {
// 			console.log(result)
// 			return res.send(true)
// 		})
// 		.catch(error => {
// 			console.log(error);
// 			res.send(false)
// 		})
// 	} else {
// 		return res.send(false)
// 	}
// }

module.exports.updateUser = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(!userData.isAdmin) {
		let updateUser = {

			
			password: req.body.password
		}
		return User.findByIdAndUpdate(userData.id, updateUser).then(result => {
			console.log(result)
			return res.send(true)
		})
		.catch(error => {
			console.log(error);
			res.send(false)
		})
	} else {
		return res.send(false)
	}
};

// module.exports.getUserOrders = (req, res) => {

	
// 	const userData = auth.decode(req.headers.authorization);
// 	console.log(userData);

// 	return User.findById(userData.id).then(result => {
// 		result.password = "";

// 		return res.send(result)
// 	})
// };