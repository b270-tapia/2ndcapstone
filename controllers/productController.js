const Product = require("../models/Product");
const auth = require("../auth");
const multer = require("multer");

// const storage = multer.diskStorage({
// 	destination: function (req, file, cb) {
// 		cb(null, './images/');

// 	},

// 	filename: function (req, file, cb) {
// 		cb(null, file.originalname);
// 	}

// });
// const upload = multer({storage: storage});


module.exports.addProduct = (req, res) => {
	
	console.log(req.file);

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		size: req.body.size,
		price: req.body.price,
		productImage: req.file.path,
		isActive: true
		
	})

	if(userData.isAdmin) {
		return newProduct.save().then(product => {
			console.log(product);
			res.send(true);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false)
	}

};

module.exports.getAllProducts = (req, res) => {
	return Product.find({}).then(result => res.send(result));
};

// Retrieving all active products

module.exports.getAllActive = (req, res) => {
	return Product.find({isActive: true}).then(result => res.send(result));
};


// Retrieve a single product

module.exports.getProduct = (req, res) => {

	console.log(req.params.productId);

	return Product.findById(req.params.productId).then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(error)
	})

};

// Updating a product

module.exports.updateProduct = (req, res) => {
	const userData = auth.decode (req.headers.authorization);
	if(userData.isAdmin) {
		let updateProduct = {

			name: req.body.name,
			description: req.body.description,
			size: req.body.size,
			price: req.body.price,
			
		}

		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new: true}).then(result => {
			console.log(result);
			res.send(result)
		})
		.catch(error => {
			console.log(error);
			res.send(false)
		})
	} else {
		return res.send(false)
		
	}
};


// Archive a Product



module.exports.archiveProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		let archiveProduct = {

			isActive: req.body.isActive
		}
		return Product.findByIdAndUpdate(req.params.productId, archiveProduct).then(result => {
			console.log(result)
			return res.send(true)
		})
		.catch(error => {
			console.log(error);
			res.send(false)
		})
	} else {
		return res.send(false)
	}
};

// module.exports.unarchiveProduct = (req, res) => {
// 	const userData = auth.decode(req.headers.authorization);
// 	if(userData.isAdmin) {
// 		let unarchiveProduct = {

// 			isActive: true
// 		}
// 		return Product.findByIdAndUpdate(req.params.productId, unarchiveProduct).then(result => {
// 			console.log(result)
// 			return res.send(true)
// 		})
// 		.catch(error => {
// 			console.log(error);
// 			res.send(false)
// 		})
// 	} else {
// 		return res.send(false)
// 	}
// }