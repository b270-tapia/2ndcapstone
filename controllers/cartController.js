const Cart = require('../models/Cart');
const User = require("../models/User");
const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");



module.exports.getAllCarts = (req, res) => {

	return Cart.find({})
		.select('-_id -products._id')
		.then(carts => {
		return res.send(carts);
		})
		.catch(err => {
			console.log(err)
			return false
		})
};

module.exports.getCartsbyUserId = (req, res) => {
	const userId = req.body.userId;
	
	return Cart.findById(userId)
		.select('-_id -products._id')
		.then(carts => {
			return res.send(carts);
		})
		.catch(err => {
			console.log(err);
			return false
		});
};

module.exports.getSingleCart = (req, res) => {

	const id = req.params.id;

	Cart.findOne({id})
		// .select('-_id -products._id')
		.then((carts) => {
			return res.send(carts);
		})
		.catch(err => {
			console.log(err);
			return false
		});
};

module.exports.addCart = (req, res) => {
	if (req.body == undefined) {
		return res.send({
			status: "error",
			message: "data is undefined",
		});
	} else {
		
		let cart = new Cart({
			userId: req.body.userId,
			products: [
					{
						productName: req.body.productName,
						productId: req.body.productId,
						quantity: req.body.quantity
					}
				]
		});
		return cart.save()
		  .then((carts) => {
			return res.send(carts);
		})
		.catch(err => {
			console.log(err);
			return false
		});

		console.log(cart);
		
	}
};

module.exports.deleteCart = (cartId) => {

	return Cart.findByIdAndRemove(cartId).then((removedCart, err) => {
		if(err) {
			console.log(err);
			return false
		} else {
			return removedCart;
		}
	})
};

module.exports.editCart = (req, res) => {
	
	
		let editCart = {

			quantity: req.body.quantity

		}
		return Cart.findOne(req.body.cartId, editCart).then(result => {
			console.log(result)
			return res.send(true)
		})
		.catch(error => {
			console.log(error);
			res.send(false)
		})
	
}

// module.exports.editCart = (req, res) => {
// 	if (req.body == undefined || req.params.id == null) {
// 		return res.send(false);
// 	} else {

// 		let editCart = {
// 			quantity: req.body.quantity
// 		}
// 		return Cart.findByIdAndUpdate(req.body.userId, editCart, {new: true}).then(result => {
// 			console.log(result);
// 			res.send(result)
// 		})
// 		.catch(error => {
// 			console.log(error);
// 			res.send(false)
// 		})
// 	}
// };



// module.exports.deleteCart = (req, res) => {
// 	if (req.body.userId == null) {
// 		return res.send({
// 			status: 'error',
// 			message: "Cart id should be provided",
// 		});
// 	} else {
// 		Cart.findById(req.body.userId)
// 			.then(cart => {
// 				return res.send(cart);
// 			})
// 			.catch(error => {
// 			console.log(error);
// 			return res.send(false)
// 		})
// 	}
// };